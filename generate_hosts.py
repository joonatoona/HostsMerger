#!/usr/bin/env python3

from urllib.request import urlopen
from sys import stdout
from re import sub, MULTILINE, DOTALL

IGNORE = ["localhost", 'localhost.localdomain', 'local', 'broadcasthost', '0.0.0.0']

IS_TTY = stdout.isatty()
R = "\033[1;31m" if IS_TTY else ""
Y = "\033[1;33m" if IS_TTY else ""
G = "\033[1;32m" if IS_TTY else ""
C = "\033[0m" if IS_TTY else "" 

print(f"{G}Downloading Lists...{C}")

BLACKLISTS = open("BLACKLIST_LIST").read().strip().split("\n")

print(f"{R}{len(BLACKLISTS)}{C} {Y}files to download{C}")

LINES = []

for blacklist in BLACKLISTS:
    print(f"{Y}Downloading{C} {blacklist}")
    lines = urlopen(blacklist.strip()).read().decode().strip().split("\n")
    print(f"{G}Parsing{C} {blacklist}")
    for line in [i.strip() for i in lines]:
        if not line or line[0] == "#":
            continue
        if "#" in line:
            line = line.split("#")[0].strip()
        if "  " in line or "\t" in line:
            line = sub("\s+", " ", line)
        line = line.split(" ")[-1]
        if line not in IGNORE and line not in LINES:
            LINES.append(line)

LINES.sort()

print(f"{G}Parsed {C}{R}{len(LINES)}{C}{G} hosts{C}")
print("Writing to hosts.txt")

OLD_HOSTS = sub("#SECTION BLACKLIST(\n|.)*#ENDSECTION", "", open("/etc/hosts").read(), MULTILINE | DOTALL)

with open("hosts.txt", "w+") as f:
    f.write(OLD_HOSTS)
    f.write("#SECTION BLACKLIST\n")
    f.write("\n".join(["0.0.0.0 " + i for i in LINES]))
    f.write("\n#ENDSECTION\n")
