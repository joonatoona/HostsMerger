# HostsMerger

Generates a hosts file from multiple sources.  

## Usage:

`./generate_hosts.py`

Hosts file sources are read from `BLACKLIST_LIST`  
Your /etc/hosts + the merged hosts are written to hosts.txt
